OS := $(shell uname)

start:
	php bin/console server:start

stop:
	php bin/console server:stop

clear:
	php bin/console cache:clear

apache:
	sudo service apache2 stop
